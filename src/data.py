import json
import shelve
import numpy as np
from sklearn.feature_extraction import DictVectorizer
import time


class Data:

    SHELF_PATH = '../data/shelf'
    X_PATH = '../data/drebin-parrot-v2-down-features-X.json'
    Y_PATH = '../data/drebin-parrot-v2-down-features-Y.json'

    def __init__(self, shelf_path=SHELF_PATH, x_path=X_PATH, y_path=Y_PATH):
        self.shelf_path = shelf_path
        self.X_path = x_path
        self.Y_Path = y_path
        self.load_data()
        pass

    def load_data(self):
        """
        Reads data, either from a pre-stored shelf or from the original json
        files.
        :return: None
        """
        try:
            # Attempt reading pre-shelved objects first
            self.__read_shelf()
        except Exception as e:
            print(f'Exception while reading the data shelf ({e})')
            # Otherwise, read data from the the json files
            self.__read_json()

    def __read_shelf(self):
        """
        Reads data as pickled objects from a pre-stored shelf.
        :return: None
        """
        with shelve.open(self.shelf_path) as shelf:
            print('Starting to read the data shelf')
            t1 = time.perf_counter()
            self.X = shelf['X']                 # Note: X is a sparse.csr_matrix
            self.X_malware = shelf['X_malware']
            self.X_goodware = shelf['X_goodware']
            self.Y = shelf['Y']
            self.feature_names = shelf['feature_names']
            t2 = time.perf_counter()
            print(f'Finished reading the data shelf. '
                  f'Elapsed time: {(t2 - t1) / 60.0} minutes')

    def __read_json(self):
        """
        Reads data from the original json files, and stores them in a shelf.
        :return: None
        """
        try:
            with open(self.X_path) as Xfile, open(self.Y_PATH) as Yfile:
                print('Starting to read the json files')
                t1 = time.perf_counter()
                Ydata = [record[0] for record in json.load(Yfile)]
                Xdata = json.load(Xfile)
                for record in Xdata:
                    del record['sha256']
                t2 = time.perf_counter()
                print(f'Finished reading the json files. '
                      f'Elapsed time: {(t2 - t1) / 60.0} minutes')
        except Exception as e:
            print(f'Exception while reading json files ({e})')
            raise e

        self.Y = np.array(Ydata, dtype=np.uint8)
        print('Y.shape', self.Y.shape)
        self.vectorizer = DictVectorizer(sparse=True, dtype=np.uint8)
        self.X = self.vectorizer.fit_transform(Xdata)
        self.feature_names = self.vectorizer.feature_names_
        print('X.shape:', self.X.shape)
        malware_idx = np.where(self.Y == 1)[0]
        self.X_malware = self.X[malware_idx, :]
        goodware_idx = np.where(self.Y == 0)[0]
        self.X_goodware = self.X[goodware_idx, :]

        try:
            with shelve.open(self.shelf_path, 'c') as shelf:
                print('Saving data to a shelf')
                shelf['X'] = self.X
                shelf['X_malware'] = self.X_malware
                shelf['X_goodware'] = self.X_goodware
                shelf['Y'] = self.Y
                shelf['feature_names'] = self.feature_names
                print('Finished saving the shelf')
        except Exception as e:
            print(f'Exception while saving data to a shelf ({e})')

    def get_X(self):
        return self.X

    def get_X_malware(self):
        return self.X_malware

    def get_X_goodware(self):
        return self.X_goodware

    def get_Y(self):
        return self.Y

    def get_feature_names(self):
        return self.feature_names