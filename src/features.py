import numpy as np
import pandas as pd
from sklearn import feature_selection
import matplotlib.pyplot as plt

pd.set_option('display.max_columns', 10)

DIR = '../data/features/'

def get_features(feature_names, weights, X, X_malware, X_goodware, Y):
    """
    Builds a feature dataframe for analysis and reporting purposes.
    :param weights: Feature weights
    :param X: Dataset features
    :param X_malware:  Malware features
    :param X_goodware: Goodware features
    :param Y: Dataset labels
    :return: features (DataFrame)
    """
    features = pd.DataFrame({'feature': feature_names, 'weight': weights})

    col_idx = X.tocsc().nonzero()[1]
    all_freq = np.bincount(col_idx) / X.shape[0]
    features['all_freq'] = all_freq

    # feature frequencies in malware
    col_idx = X_malware.tocsc().nonzero()[1]
    mal_freq = np.bincount(col_idx) / X_malware.shape[0]
    features['mal_freq'] = mal_freq

    # feature frequencies in goodware
    col_idx = X_goodware.tocsc().nonzero()[1]
    good_freq = np.bincount(col_idx) / X_goodware.shape[0]
    features['good_freq'] = good_freq

    chi2, _ = feature_selection.chi2(X, Y)
    features['chi2'] = chi2

    # features.to_csv(DIR + 'features.csv')

    return features

def weight_summary(features):
    """
    Outputs a summary of weight distribution.
    :param features:
    :return: None
    """
    weights = features[['weight']]
    summary = weights.describe()

    fig, (ax1, ax2) = plt.subplots(1, 2)

    bg = '#ACDCF6'
    ax1.axis("off")
    tbl = ax1.table(cellText=np.round(summary.values, 4), loc='center',
            rowLabels=summary.index, colLabels=summary.columns,
            rowColours=['w', bg, bg, bg, 'w', 'w', 'w', bg],
            cellColours=[['w'], [bg], [bg], [bg], ['w'], ['w'], ['w'], [bg]])

    tbl.auto_set_font_size(False)
    tbl.set_fontsize(14)
    tbl.scale(1.25, 1.25)

    weights.plot(ax=ax2, kind='kde',
                    xlim=(weights.weight.min(), weights.weight.max()))

    plt.tight_layout()
    plt.show()


def feature_report(features, X, X_malware, X_goodware, Y):
    print('1. Dataframe of all features\n')
    print(f'Columns:\n--------\n'
          f'0 {"idx":<10} => Index\n'
          f'1 {"feature":<10} => Feature name\n'
          f'2 {"weight":<10} => Feature weight in the trained model\n'
          f'3 {"all_freq":<10} => P(x|D) The normalised frequency of the feature in the whole dataset\n'
          f'4 {"mal_freq":<10} => P(x|malware) The normalised frequency of the feature in the malware class\n'
          f'5 {"good_freq":<10} => P(x|goodware) The normalised frequency of the feature in the goodware class\n'
          f'6 {"chi2":<10} => The chi^2 value to measure the independence between features and classes (for feature selection)\n')
    print(features)
    # features.to_csv(DIR + 'features.csv')

    # Rank by chi2 values, for feature selection
    top = 50000
    top_features_chi2 = features.sort_values(by='chi2', ascending=False)
    print(f'\n2. Top {top} features, ranked by Chi2 (for feature selection):')
    print(top_features_chi2.head(top))
    # top_features_chi2.to_csv(DIR + 'top_features_chi2.csv')

    nz_features = features.loc[features.weight != 0]
    print(f'\n3. Features with non-zero weights '
          f'({nz_features.shape[0]}/{features.shape[0]} = '
          f'{nz_features.shape[0]/features.shape[0]*100:.2f}%):')
    print(nz_features)
    # nz_features.to_csv(DIR + 'nz_features.csv')

    # Rank by the magnitude of weights (absolute values)
    ranked_idx = np.argsort(abs(nz_features.weight)).values[::-1]
    abs_features = nz_features.iloc[ranked_idx]
    top = 50000
    print(f'\n4. Top {top} features, '
          f'Ranked by weight magnitude (absolute values):')
    print(abs_features.head(top))
    # abs_features.to_csv(DIR + 'abs_nz_features.csv')

    # Rank features with positive weights
    pos_features = nz_features.loc[nz_features.weight > 0]
    pos_features = pos_features.sort_values(by='weight', ascending=False)
    top = 50000
    print(f'\n5. Top {top} positive features (out of {pos_features.shape[0]}), '
          f'Ranked by weight:')
    print(pos_features.head(top))
    # pos_features.to_csv(DIR + 'pos_nz_features.csv')

    # Rank features with negative weights
    neg_features = nz_features.loc[nz_features.weight < 0]
    neg_features = neg_features.sort_values(by='weight', ascending=True)
    top = 50000
    print(f'\n6. Top {top} negative features (out of {neg_features.shape[0]}), '
          f'Ranked by weight:')
    print(neg_features.head(top))
    # neg_features.to_csv(DIR + 'neg_nz_features.csv')