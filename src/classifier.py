from sklearn.model_selection import GridSearchCV
from sklearn.svm import LinearSVC
import os
import joblib
from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import plot_confusion_matrix
import matplotlib.pyplot as plt

C_1_PATH = '../data/c_1_svm.joblib'
C_BEST_PATH = '../data/c_best_svm.joblib'
C_VALS = [0.001, 0.01, 0.1, 1, 10, 100, 1000]

def fit_clf(X_train, Y_train, tune=False, C_values=C_VALS):
    """
    Trains a linear SVM model, either with C=1 (default tune=False) or with
    a tuned C as determined by a grid search over a set of C values
    :param X_train: Training set features
    :param Y_train: Training set labels
    :param tune: Whether to tune the hyperparameter C or use the default C=1
    :param C: The set of C values to try in the grid search.
    :return: Fitted SVM model.
    """
    if not tune:
        if os.path.isfile(C_1_PATH):
            clf = joblib.load(C_1_PATH)
            print("SVM-C1 Loaded")
        else:
            print("Training SVM-C1")
            clf = LinearSVC(penalty='l2', loss='hinge', C=1.0, max_iter=1e6,
                            verbose=1)
            clf.fit(X_train, Y_train)
            joblib.dump(clf, C_1_PATH)
            print("Finished Training SVM-C1")
    else:
        if os.path.isfile(C_BEST_PATH):
            clf = joblib.load(C_BEST_PATH)
            print("SVM-C-Best Loaded")
        else:
            print("Tuning Hyperparameter C with Grid Search & Cross Validation")
            params = {'C': C_values}
            clf = GridSearchCV(
                LinearSVC(penalty='l2', loss='hinge', max_iter=1e6, verbose=1),
                params, cv=5, scoring='f1', n_jobs=-1, verbose=10)

            clf = clf.fit(X_train, Y_train)
            clf = clf.best_estimator_
            print("Best model:", clf)
            joblib.dump(clf, C_BEST_PATH)

    return clf


def evaluate(model, X, Y_true):
    """
    Computes a number of evaluation metrics for a given model, and outputs the
    results.
    :param model: The model to be evaluated.
    :param X: Test dataset.
    :param Y_true: True labels of the test dataset.
    :return: None
    """
    Y_pred = model.predict(X)

    accuracy = accuracy_score(Y_true, Y_pred)
    report = classification_report(Y_true, Y_pred,
                                   labels=[1, 0],
                                   target_names=['Malware', 'Goodware'])
    print("Classification Report:\n", report)
    TN, FP, FN, TP = confusion_matrix(Y_true, Y_pred).ravel()
    print('[TP, FN, FP, TN] =', [TP, FN, FP, TN])

    f, (ax1, ax2) = plt.subplots(1, 2, sharey='row', figsize=(12, 5))
    f.suptitle("Confusion Matrix", fontsize=16)
    f.text(0.4, 0, 'Predicted label', ha='left', fontsize=14)
    f.text(0, 0.5, 'True label', ha='left', va='center', fontsize=14,
           rotation='vertical')
    plots = [(1, "Without Normalisation", None, 'd', ax1, True),
             (2, "With Normalisation", 'true', '.2f', ax2, False)]

    for i, title, normalize, fmt, ax, ylabel in plots:
        disp = plot_confusion_matrix(model, X, Y_true,
                                     labels=[1, 0],
                                     display_labels=['Malware', 'Goodware'],
                                     normalize=normalize, values_format=fmt,
                                     ax=ax, cmap=plt.cm.Blues)
        disp.ax_.set_title(title)
        disp.ax_.set_xlabel('')
        disp.ax_.set_ylabel('')

    plt.show()
